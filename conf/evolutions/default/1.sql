# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table student (
  id                        bigint not null,
  msv                       varchar(255),
  name                      varchar(255),
  khoa                      varchar(255),
  birthday                  varchar(255),
  email                     varchar(255),
  tendt                     varchar(255),
  user_id                   bigint,
  constraint pk_student primary key (id))
;

create table user_account (
  id                        bigint not null,
  msv                       varchar(255),
  name                      varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  cv                        varchar(255),
  constraint pk_user_account primary key (id))
;

create sequence student_seq;

create sequence user_account_seq;

alter table student add constraint fk_student_user_1 foreign key (user_id) references user_account (id);
create index ix_student_user_1 on student (user_id);



# --- !Downs

drop table if exists student cascade;

drop table if exists user_account cascade;

drop sequence if exists student_seq;

drop sequence if exists user_account_seq;

