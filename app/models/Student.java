package models;

import java.util.ArrayList;
import java.util.List;
import play.data.validation.ValidationError;

import play.db.ebean.Model;
import com.avaje.ebean.Page;
import javax.persistence.Id;
import play.data.format.Formats;
import play.mvc.PathBindable;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import scala.util.Either;
import javax.persistence.Entity;
import javax.persistence.*;

import javax.persistence.OneToOne;

@Entity
public class Student extends Model implements PathBindable<Student>{

    @Id
    public Long id;

    public String msv="";

    public String name="";

    public String khoa="";

    public String birthday="";

    public String email="";


    public String tendt="";

    @OneToOne
    public UserAccount user;


    public static Finder<Long,Student> find = new Finder<Long,Student>(
            Long.class, Student.class
    );

    public static Page<Student> find(int page) {  // trả về trang thay vì List
        return find.where()
                .orderBy("msv")     // sắp xếp tăng dần theo id
                .findPagingList(10)    // quy định kích thước của trang
                .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
                .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
    }

    public Student() {
        msv = "";
        name = "";
        birthday = "";
        email = "";
        tendt = "";
    }

    public Student(String msv, String name, String email) {
        this.msv = msv;
        this.name = name;
        birthday = "";
        this.email = email;
        tendt = "";
    }

    public Student(String msv, String name, String birthday, String khoa, String email, String tendt) {
        this.msv = msv;
        this.name = name;
        this.birthday = birthday;
        this.khoa = khoa;
        this.email = email;
        this.tendt = tendt;
    }

    public String toString() {
        return String.format("%s - %s", msv, name);
    }

    private static List<Student> students;

    public static List<Student> findAll() {
        return find.all();
    }

    public static Student findByMsv(String msv) {
        return find.where().eq("msv", msv).findUnique();
    }
    public static Student findByEmail(String email) {
        return find.where().eq("email", email).findUnique();
    }
    public static Student findByName(String name) {
        return find.where().eq("name", name).findUnique();
    }


    @Override
    public Student bind(String key, String value) {
        return findByMsv(value);
    }

    @Override
    public String unbind(String key) {
        return msv;
    }

    @Override
    public String javascriptUnbind() {
        return msv;
    }
    public List<ValidationError> validate() {

        List<ValidationError> errors = new ArrayList<ValidationError>();

        if (name == null || name.length() == 0) {
            errors.add(new ValidationError("name", "Please enter your first and last name. (required)"));
        }

        if(errors.size() > 0)
            return errors;

        return null;
    }
}
