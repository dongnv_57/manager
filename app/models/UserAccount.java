package models;

import java.util.ArrayList;
import java.util.List;
import com.avaje.ebean.Page;

import play.mvc.PathBindable;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import scala.util.Either;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.data.validation.ValidationError;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
/**
 * Created by dse on 2/22/15.
 */
@Entity
public class UserAccount extends Model implements PathBindable<UserAccount>{

    @Id
    public Long id;

    public String msv="";

    public String name="";

    public String email="";

    public String password="";

    public String cv = "student";

    @OneToOne(mappedBy = "user")
    public Student student;

    public UserAccount() { }
    public UserAccount(String msv, String name, String email, String password, String cv) {
        this.msv = msv;
        this.name = name;
        this.email = email;
        this.password = password;
        this.cv = cv;
    }

    public static UserAccount authenticate(String email, String password) {
        return finder.where().eq("email", email)
                .eq("password", password).findUnique();
    }

    public static Finder<Long,UserAccount> find = new Finder<Long,UserAccount>(
            Long.class, UserAccount.class
    );

    public static Finder<Long, UserAccount> finder = new Finder<Long, UserAccount>(
            Long.class, UserAccount.class);

    public static List<UserAccount> findAll() {
        return finder.all();
    }

    public static UserAccount findByName(String name) {
        return finder.where().eq("name", name).findUnique();
    }

    public static UserAccount findByMsv(String msv) {
        return find.where().eq("msv", msv).findUnique();
    }
    public static UserAccount findByEmail(String email) {
        return find.where().eq("email", email).findUnique();
    }

    @Override
    public UserAccount bind(String key, String value) {
        return findByMsv(value);
    }

    @Override
    public String unbind(String key) {
        return msv;
    }

    @Override
    public String javascriptUnbind() {
        return msv;
    }

    public static Page<UserAccount> find(int page) {
        return finder.where().orderBy("cv").findPagingList(5)
                .setFetchAhead(false).getPage(page);

    }

    public static UserAccount findById(Long id) {
        return finder.byId(id);
    }
    public List<ValidationError> validate() {

        List<ValidationError> errors = new ArrayList<ValidationError>();
        if (msv == null || msv.length() == 0) {
            errors.add(new ValidationError("msv", ""));
        }

        if (name == null || name.length() == 0) {
            errors.add(new ValidationError("name", "Please enter your first and last name. (required)"));
        }
        if (email == null || email.equals("")) {
            errors.add(new ValidationError("email", "Please enter email"));
        }
        if (password == null || password.length() == 0) {
            errors.add(new ValidationError("password", "Please enter your password. (required)"));
        }
        if(errors.size() > 0)
            return errors;

        return null;
    }
}

