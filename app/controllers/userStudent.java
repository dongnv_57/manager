package controllers;
import com.avaje.ebean.Ebean;
import play.db.ebean.Model;
import com.avaje.ebean.Page;
import play.mvc.Controller;
import play.mvc.Result;
import models.*;

import java.util.ArrayList;
import java.util.List;

import views.html.students.details;
import views.html.accounts.*;
import views.html.students.*;
import views.html.catalog;

import play.data.Form;
import play.mvc.Security;


@Security.Authenticated(Secured.class)
public class userStudent extends Controller {
    private static final Form<Student> studentForm = Form.form(Student.class);
    private static final Form<UserAccount> userForm = Form.form(UserAccount.class);

    public static Result save() {
        Form<Student> boundForm = studentForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(details2.render(boundForm));
        }
        Student student = boundForm.get();
        if (student.id == null) {
            if (Student.findByMsv(student.msv) == null) {
                flash("success", String.format("Thên mới thành công"));
                student.save();
            }
        } else {
            student.update();
            flash("success", String.format("Cập nhật thành công"));
        }
        return redirect(routes.userStudent.tt(student));
    }
    public static Result tt(Student student) {
        if (student == null) {
            return notFound(String.format("Student does not exist."));
        }
        Form<Student> filledForm = studentForm.fill(student);
        return ok(views.html.students.details.render(filledForm));
    }

    public static Result tk(UserAccount user) {
        if (user == null) {
            return notFound(String.format("Account does not exist."));
        }
        Form<UserAccount> filledForm = userForm.fill(user);
        return ok(views.html.changeAccount.render(filledForm));
    }

    public static Result changeTt(Student student) {
        if (student == null) {
            return notFound(String.format("Student does not exist."));
        }
        Form<Student> filledForm = studentForm.fill(student);
        return ok(views.html.students.details2.render(filledForm));
    }

    public static Result changeTk(UserAccount user) {
        if (user == null) {
            return notFound(String.format("Student %s does not exist.", user.name));
        }
        Form<UserAccount> filledForm = userForm.fill(user);
        return ok(views.html.accounts.details_account.render(filledForm));
    }
}
