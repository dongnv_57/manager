package controllers;
import com.avaje.ebean.Ebean;
import play.db.ebean.Model;
import com.avaje.ebean.Page;
import play.mvc.Controller;
import play.mvc.Result;
import models.*;
import views.html.accounts.*;
import views.html.ttStudent;

import java.util.ArrayList;
import java.util.List;
import views.html.students.details;
import play.data.Form;
import play.mvc.Security;


@Security.Authenticated(Secured.class)
public class Students extends Controller {
    private static final Form<Student> studentForm = Form.form(Student.class);
    private static final Form<UserAccount> userForm = Form.form(UserAccount.class);

    public static Result list(Integer page) {
        Page<Student> students = Student.find(page);
        return ok(views.html.catalog.render(students));
    }
    public static Result newStudent(){
        return ok(details.render(studentForm));
    }

    public static Result save() {
        Form<UserAccount> boundForm = userForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(views.html.accounts.details_account3.render(boundForm));
        }
        UserAccount account = boundForm.get();
        if (account.id == null) {
            if(UserAccount.findByMsv(account.msv) == null) {
                flash("suscess", "Thành công");
                account.save();
            }
        } else {
            account.update();
            flash("success", String.format("Cập nhật thành công"));
        }
        return redirect(routes.userStudent.tk(account));
    }
    public static Result tt(Student student) {
        if (student == null) {
            return notFound(String.format("Student does not exist."));
        }
        Form<Student> filledForm = studentForm.fill(student);
        return ok(views.html.ttStudent.render(filledForm));
    }
    public static Result delete(String msv) {
        final Student student = Student.findByMsv(msv);
        if(student == null) {
            return notFound(String.format("Student %s does not exists.", msv));
        }
        student.delete();
        return redirect(routes.Students.list(0));
    }
}
