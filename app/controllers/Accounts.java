package controllers;
import com.avaje.ebean.Ebean;
import play.db.ebean.Model;
import com.avaje.ebean.Page;
import play.mvc.Controller;
import play.mvc.Result;
import models.*;
import java.util.ArrayList;
import java.util.List;

import views.html.*;
import views.html.accounts.*;

import views.html.students.*;

import play.data.Form;
import play.mvc.Security;


@Security.Authenticated(Secured.class)
public class Accounts extends Controller {
    private static final Form<UserAccount> userForm = Form.form(UserAccount.class);

    public static Result list(Integer page) {
        Page<UserAccount> users = UserAccount.find(page);
        return ok(views.html.catalog_account.render(users));
    }

    public static Result newAccount(){
        return ok(views.html.accounts.details_account3.render(userForm));
    }

    public static Result details(UserAccount user) {
        if (user == null) {
            return notFound(String.format("Student %s does not exist.", user.name));
        }
        Form<UserAccount> filledForm = userForm.fill(user);
        return ok(views.html.accounts.details_account2.render(filledForm));
    }

    public static Result save() {
        Form<UserAccount> boundForm = userForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(views.html.accounts.details_account3.render(boundForm));
        }
        UserAccount account = boundForm.get();
            if (account.id == null) {
                if (UserAccount.findByName(account.name) == null) {
                    Student student = new Student();
                    account.save();
                    student.msv = account.msv;
                    student.email = account.email;
                    student.save();
                    flash("success", String.format("Tạo thành công"));
                }
            } else {
                account.update();
                flash("success", String.format("Cập nhật thành công"));
            }
        return redirect(routes.Accounts.list(0));
    }

    public static Result delete(String msv) {
        final UserAccount user = UserAccount.findByMsv(msv);
        if(user == null) {
            return notFound(String.format("Account %s does not exists.", msv));
        }
        flash("success", String.format("Xoá thành công "));
        user.delete();

        Student student = Student.findByMsv(msv);
        student.delete();
        return redirect(routes.Accounts.list(0));
    }
}
