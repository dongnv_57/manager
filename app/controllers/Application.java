package controllers;

import models.Student;
import models.UserAccount;
import play.data.Form;
import play.mvc.*;
import static play.data.Form.form;

import views.html.*;
import views.html.student;

import views.html.home;

import models.*;
import play.data.validation.Constraints;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public static Result home() {
        return ok(views.html.home.render());
    }
    public static Result home2(String msv){ return ok(views.html.student.render(msv));}
    public static Result login() {
        return ok(
                login.render(form(Login.class))
        );
    }

    public static Result logout() {
        session().clear();
        return redirect(routes.Application.login());
    }

    public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        String email = loginForm.get().email;
        String password = loginForm.get().password;
        session().clear();
        UserAccount userAccount = UserAccount.authenticate(email, password);

        if (userAccount == null) {
            flash("error", "Thông tin tài khoản/Mật khẩu không chính xác");
            return redirect(routes.Application.login());
        }
        String cv = userAccount.cv;
        session("email", email);
        session("cv", cv);
        switch (cv) {
            case "student": {
                return ok(student.render(Student.findByEmail(email).msv));
            }
            case "admin": {
                return ok(home.render());
            }
        }
        return TODO;
    }


    public static class Login{
        @Constraints.Required
        public String email;

        @Constraints.Required
        public String password;
    }
}
